import json
from zoro_data_access.product_cache import QueryBuilder


def get_product_generator(**kwargs):
    query_builder = QueryBuilder(kwargs.get("query_url", ""), kwargs.get("index_name", ""))
    product_query = query_builder.query_products()
    return product_query.result_generator()


def fetch_es_fields(es_url, es_index, destination, fields=[], exclusive=False):
    product_cache_p = get_product_generator(query_url=es_url, index_name=es_index, doc_type="zitem")
    product_cache_csv = open(destination, "w")
    for product_p in product_cache_p:
        product_json = json.loads(product_p.to_json())
        write_doc = {"zoroNo": product_json["zoroNo"]}
        for field in fields:
            if field in product_json:
                write_doc[field] = product_json[field]
        if (exclusive and len(fields) > 0 and len(write_doc.keys()) > 1) or not exclusive or len(fields) < 1:
            product_cache_csv.write(json.dumps(write_doc, sort_keys=True) + '\n')
    product_cache_csv.close()
    return destination
