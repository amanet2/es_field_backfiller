import os

settings = {
    "es_url_old": os.getenv("MEDIA_BACKFILL_ES_URL_OLD"),
    "es_index_old": os.getenv("MEDIA_BACKFILL_ES_INDEX_OLD"),
    "es_url_new": os.getenv("MEDIA_BACKFILL_ES_URL_NEW"),
    "es_index_new": os.getenv("MEDIA_BACKFILL_ES_INDEX_NEW"),
    "es_local_dump_folder": os.getenv("MEDIA_BACKFILL_ES_LOCAL_DUMP_FOLDER", "tmp"),
    "logging_app_name": os.getenv("MEDIA_BACKFILL_LOGGING_APP_NAME", "MEDIA_BACKFILL"),
    "logging_level": os.getenv("MEDIA_BACKFILL_LOGGING_LEVEL", "INFO"),
    "backfill_fields": os.getenv("MEDIA_BACKFILL_BACKFILL_FIELDS", "media"),
    "backfill_mode": os.getenv("MEDIA_BACKFILL_BACKFILL_MODE", "replace"),
    "filepart_line_count": int(os.getenv("MEDIA_BACKFILL_FILEPART_LINE_COUNT", 4200))
}
