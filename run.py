import sys
import traceback
import logging
from zoro_logging import stdout
from config import settings
from utils import refresh_dirs, remove_leading_slash, split_file, delete_file
from es_zitem_dump import fetch_es_fields
from es_upsert_creator import create_upsert_doc

logger = logging.getLogger(settings["logging_app_name"])
logger.setLevel(settings["logging_level"])
logger.addHandler(stdout.get_handler())


def es_compare(**kwargs):
    try:
        es_url_old = kwargs.get("es_url_old")
        es_index_old = kwargs.get("es_index_old")
        es_url_new = kwargs.get("es_url_new")
        es_index_new = kwargs.get("es_index_new")
        local_dump_folder = kwargs.get("es_local_dump_folder")
        filepart_line_count = kwargs.get("filepart_line_count")
        backfill_fields = kwargs.get("backfill_fields", "media").strip().split(",")

        dump_old = remove_leading_slash("{}/old".format(local_dump_folder))
        dump_new = remove_leading_slash("{}/new".format(local_dump_folder))
        refresh_dirs(local_dump_folder)
        logger.info("DOWNLOADING {}/{}".format(es_url_old, es_index_old))
        fetch_es_fields(es_url=es_url_old, es_index=es_index_old, destination=dump_old, fields=backfill_fields,
                        exclusive=True)
        logger.info("DOWNLOADING {}/{}".format(es_url_new, es_index_new))
        fetch_es_fields(es_url=es_url_new, es_index=es_index_new, destination=dump_new, fields=[])
        logger.info("PROCESSING DATA")
        fileparts_old = split_file(source=dump_old, destination="{}.".format(dump_old), lines=filepart_line_count)
        delete_file(dump_old)
        fileparts_new = split_file(source=dump_new, destination="{}.".format(dump_new), lines=filepart_line_count)
        delete_file(dump_new)
        logger.info("CREATING UPSERT DOC")
        create_upsert_doc(fileparts_old, fileparts_new, "")
    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        logger.error(traceback.format_exception(exc_type, exc_value, exc_traceback))


if __name__ == "__main__":
    logger.info("STARTING UP")
    es_compare(**settings)
    logger.info("DONE")
