import os
import platform
import shutil
import subprocess
import json
import glob
from datetime import datetime


def get_os():
    return platform.system().lower()


def get_file_line_count(filename):
    cmd = "wc -l {}".format(filename)
    res = subprocess.check_output(cmd, shell=True).decode("utf-8").split(" ")
    current_os = get_os()
    if "darwin" in current_os:
        return int(res[1])  # implied mac os
    else:
        return int(res[0])


def is_file_sorted(filename):
    fp = open(filename, "r")
    data = []
    for line in fp:
        data.append(json.loads(line)["zoroNo"])
    fp.close()
    ind = 0
    while True:
        if data[ind] > data[min(len(data) - 1, ind + 1)]:
            return False
        ind += 1
        if ind > len(data)-1:
            return True


def partition_file(source, destination, parts):
    lines = int(get_file_line_count(source)/parts)
    return split_file(source, destination, lines)


def split_file(source, destination, lines):
    cmd = "split"
    if "darwin" in get_os():
        cmd = "gsplit"
    completed_proc = subprocess.run("{} -a 4 -l {} -d {} {}".format(cmd, lines, source, destination), shell=True)
    return glob.glob(destination+"*")


def refresh_dirs(dirs):
    if isinstance(dirs, str):
        dirs = [dirs]
    for d in dirs:
        if os.path.isdir(d):
            shutil.rmtree(d)
        os.mkdir(d)


def delete_file(filename):
    if os.path.exists(filename):
        os.remove(filename)


def remove_leading_slash(path):
    if path[0] == '/':
        path = path[1:]
    return path


def get_unique_id():
    return datetime.utcnow().strftime("%Y%m%d%H%M%S")
