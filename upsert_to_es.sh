source ./config.sh

for batch in ./upsert_doc.json.single; do
	curl -XPOST -H "Content-type: application/x-ndjson" "${MEDIA_BACKFILL_ES_URL_NEW}/_bulk" --data-binary "@${batch}"
done