import json

key_separator = "^^^"  # choose wisely


def get_file_lines_as_list(filename):
    open_file = open(filename, "r")
    file_lines = open_file.readlines()
    open_file.close()
    return file_lines


def get_data_dict_from_lines(lines):
    lines_dict = {}
    for product_str in lines:
        product_json = json.loads(product_str)
        lines_dict[product_json["zoroNo"]] = product_json
    return lines_dict


def create_sku_map(file_parts=[]):
    sku_map = {}
    for filename in sorted(file_parts):
        with open(filename, "r") as fp:
            lines = fp.readlines()
            json_first = json.loads(lines[0])
            json_last = json.loads(lines[len(lines)-1])
            newkey = "{}{}{}".format(json_first["zoroNo"], key_separator, json_last["zoroNo"])
            sku_map[newkey] = filename
    return sku_map


def create_upsert_doc(files_old=[], files_new=[], destination="", mode="replace"):
    if mode not in ["merge", "replace"]:
        mode="replace"
    if len(destination) > 0 and destination[len(destination)-1] != '/':
        destination = destination + "/"
    if isinstance(files_old, str):
        files_old = [files_old]
    if isinstance(files_new, str):
        files_new = [files_new]
    final_destination = "{}{}".format(destination, "upsert_doc.json")
    upsert_doc = open(final_destination, "w")
    files_old = sorted(files_old)
    files_new = sorted(files_new)
    sku_map_new = create_sku_map(files_new)
    sku_range_pointer = ""
    product_dict_new = {}
    for i in range(0, len(files_old)):
        file_old = files_old[i]
        file_old_sorted_lines = get_file_lines_as_list(file_old)
        for product_str_old in file_old_sorted_lines:
            product_json_old = json.loads(product_str_old)
            search_sku = product_json_old["zoroNo"]
            found_range = False
            if search_sku not in product_dict_new.keys():
                for sku_range in sku_map_new.keys():
                    lower = sku_range.split(key_separator)[0]
                    upper = sku_range.split(key_separator)[1]
                    if (upper >= lower and search_sku >= lower and search_sku <= upper)\
                    or (upper < lower and search_sku >= lower) or (upper < lower and search_sku <= upper):
                        found_range = True
                        if sku_range_pointer != sku_range:
                            sku_range_pointer = sku_range
                            product_dict_new = get_data_dict_from_lines(
                                get_file_lines_as_list(sku_map_new[sku_range_pointer]))
                        break
                if not found_range:
                    continue
            print("{} - {} - {}".format(search_sku, sku_range_pointer, sku_map_new[sku_range_pointer]))
            if search_sku in product_dict_new.keys():
                for field in product_json_old.keys():
                    if field != "zoroNo":
                        product_json_new = {}
                        if mode == "merge":
                            product_json_new = product_dict_new[search_sku]
                        if mode == "replace" or (mode == "merge" and field not in product_json_new.keys()):
                            update_obj = {'update': {'_id': search_sku, '_type': 'zitem',
                                                     '_index': 'zoro_4', 'retry_on_conflict': 1}}
                            doc_obj = {'doc': {field: product_json_old[field]}, 'doc_as_upsert': True}
                            upsert_doc.write(json.dumps(update_obj, sort_keys=True) + '\n')
                            upsert_doc.write(json.dumps(doc_obj, sort_keys=True) + '\n')
    upsert_doc.close()
    return final_destination
